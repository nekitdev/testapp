package com.nekitdev.testapp

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.nekitdev.testapp.ui.screens.activities.main.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    private val ip = "24.48.0.1"
    private val wrongIp = "24.48.0.1.00.00.00"

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, true, true)

    @Test
    fun startTest() {
        checkIp()
    }

    private fun checkIp() {
        fillEditText(R.id.etIp, ip)
        clickOnView(R.id.btnGetData)
        clearEditText(R.id.etIp)
        fillEditText(R.id.etIp, wrongIp)
        clickOnView(R.id.btnGetData)
    }
}