package com.nekitdev.testapp

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers

fun ViewInteraction.pause(seconds: Int = 1) {
    Thread.sleep(seconds * 1000L)
}

fun clickOnView(@IdRes res: Int) {
    Espresso.onView(ViewMatchers.withId(res)).perform(click()).pause()
}

fun fillEditText(@IdRes res: Int, text: String) {
    Espresso.onView(ViewMatchers.withId(res)).perform(
        typeText(text),
        closeSoftKeyboard()
    ).pause()
}

fun clearEditText(@IdRes res: Int) {
    Espresso.onView(ViewMatchers.withId(res)).perform(clearText())
}