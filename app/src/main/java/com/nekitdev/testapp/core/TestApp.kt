package com.nekitdev.testapp.core

import android.app.Application
import com.nekitdev.testapp.di.repositoryModule
import com.nekitdev.testapp.di.useCasesModule
import com.nekitdev.testapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TestApp)
            modules(listOf(repositoryModule(), useCasesModule(), viewModelModule()))
        }
    }
}