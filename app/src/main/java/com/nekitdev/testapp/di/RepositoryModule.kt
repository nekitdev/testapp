package com.nekitdev.testapp.di

import com.nekitdev.data.api.IpApi
import com.nekitdev.data.api.RetrofitProvider
import com.nekitdev.data.repository.IpRepositoryImpl
import com.nekitdev.domain.repository.IpRepository
import org.koin.dsl.module
import retrofit2.Retrofit

fun repositoryModule() = module {
    single<RetrofitProvider> {
        RetrofitProvider()
    }

    single<Retrofit> {
        get<RetrofitProvider>().retrofit
    }

    single<IpApi> {
        val retrofit = get<Retrofit>()
        retrofit.create(IpApi::class.java)
    }

    single<IpRepository> {
        IpRepositoryImpl(api = get())
    }
}