package com.nekitdev.testapp.di

import com.nekitdev.domain.usecase.IpUseCase
import org.koin.dsl.module

fun useCasesModule() = module {

    single<IpUseCase> {
        IpUseCase(repository = get())
    }
}