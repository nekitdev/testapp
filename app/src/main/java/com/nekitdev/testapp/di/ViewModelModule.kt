package com.nekitdev.testapp.di

import com.nekitdev.testapp.ui.screens.activities.main.MainVM
import com.nekitdev.testapp.ui.screens.activities.main.MainVMContract
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun viewModelModule() = module {

    viewModel<MainVMContract> {
        MainVM(ipUseCase = get())
    }
}