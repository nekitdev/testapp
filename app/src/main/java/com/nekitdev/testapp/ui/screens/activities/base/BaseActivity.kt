package com.nekitdev.testapp.ui.screens.activities.base

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.nekitdev.testapp.R

abstract class BaseActivity<VM : BaseVM> : AppCompatActivity() {

    abstract val layoutRes: Int
    abstract val viewModel: VM
    abstract fun buttonClickListener()
    abstract fun liveDataObserver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        buttonClickListener()
        liveDataObserver()
        observeToError()
    }

    private fun observeToError() {
        viewModel.publicExceptionHandlerLiveData.observe(this, Observer {
            Toast.makeText(this, resources.getString(R.string.some_error), Toast.LENGTH_SHORT)
                .show()
        })
    }
}