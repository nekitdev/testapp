package com.nekitdev.testapp.ui.screens.activities.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*


abstract class BaseVM : ViewModel(), CoroutineScope {

    val publicExceptionHandlerLiveData = MutableLiveData<Throwable>()

    override val coroutineContext =
        SupervisorJob() + Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            baseExceptionHandler(throwable)
        }

    private fun baseExceptionHandler(throwable: Throwable) {
        Log.e("Error", throwable.message, throwable)
        publicExceptionHandlerLiveData.postValue(throwable)
    }

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancel()
    }
}