package com.nekitdev.testapp.ui.screens.activities.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.lifecycle.Observer
import com.nekitdev.testapp.R
import com.nekitdev.testapp.ui.screens.activities.base.BaseActivity
import com.nekitdev.testapp.utils.text
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<MainVMContract>() {

    override val layoutRes = R.layout.activity_main
    override val viewModel: MainVMContract by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun buttonClickListener() {
        btnGetData.setOnClickListener { validateInput() }
    }

    override fun liveDataObserver() {
        observeToError()
        observeToDataByIp()
    }

    private fun observeToError() {
        viewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    @SuppressLint("SetTextI18n")
    private fun observeToDataByIp() {
        viewModel.ipDataLD.observe(this, Observer {
            tvQuery.text = "${resources.getString(R.string.query)} ${it.query}"
            tvStatus.text = "${resources.getString(R.string.status)} ${it.status}"
            tvCountry.text = "${resources.getString(R.string.country)} ${it.country}"
            tvCountryCode.text = "${resources.getString(R.string.country_сode)} ${it.countryCode}"
            tvRegion.text = "${resources.getString(R.string.region)} ${it.region}"
            tvRegionName.text = "${resources.getString(R.string.region_name)} ${it.regionName}"
            tvCity.text = "${resources.getString(R.string.city)} ${it.city}"
            tvZip.text = "${resources.getString(R.string.zip)} ${it.zip}"
            tvLat.text = "${resources.getString(R.string.lat)} ${it.lat}"
            tvLon.text = "${resources.getString(R.string.lon)} ${it.lon}"
            tvTimezone.text = "${resources.getString(R.string.timezone)} ${it.timezone}"
            tvIsp.text = "${resources.getString(R.string.isp)} ${it.isp}"
            tvOrg.text = "${resources.getString(R.string.org)} ${it.org}"
            tvAs.text = "${resources.getString(R.string.`as`)} ${it.`as`}"
        })
    }

    private fun validateInput() {
        if (Patterns.IP_ADDRESS.matcher(etIp.text()).matches()) {
            viewModel.getDataByIp(etIp.text())
        } else {
            Toast.makeText(this, resources.getString(R.string.not_valid_ip), Toast.LENGTH_SHORT)
                .show()
        }
    }
}