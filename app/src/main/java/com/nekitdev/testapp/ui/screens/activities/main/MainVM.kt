package com.nekitdev.testapp.ui.screens.activities.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nekitdev.domain.models.CustomResult
import com.nekitdev.domain.models.localmodels.IpLocalModel
import com.nekitdev.domain.usecase.IpUseCase
import com.nekitdev.testapp.ui.screens.activities.base.BaseVM
import kotlinx.coroutines.launch

abstract class MainVMContract() : BaseVM() {
    abstract fun getDataByIp(ip: String)

    abstract val errorMessage: LiveData<String>
    abstract val ipDataLD: LiveData<IpLocalModel>

}

class MainVM(private val ipUseCase: IpUseCase) : MainVMContract() {

    override val errorMessage = MutableLiveData<String>()
    override val ipDataLD = MutableLiveData<IpLocalModel>()

    override fun getDataByIp(ip: String) {
        launch {
            val response = ipUseCase.getDataByIp(ip)
            when (response) {
                is CustomResult.Success -> {
                    ipDataLD.postValue(response.data)
                }
                is CustomResult.Failure -> {
                    errorMessage.postValue(response.error.message)
                }
            }
        }
    }
}

