package com.nekitdev.testapp.utils

import android.widget.EditText

fun EditText.text(): String = this.text.toString()