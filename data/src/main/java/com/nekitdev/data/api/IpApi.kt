package com.nekitdev.data.api

import com.nekitdev.data.api.models.response.IpResponseModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface IpApi {

    @GET("json/{query}")
    suspend fun getDataByIp(@Path("query") ip: String): Response<IpResponseModel>
}