package com.nekitdev.data.api.models.response

import com.nekitdev.domain.models.localmodels.IpLocalModel

data class IpResponseModel(
    val `as`: String,
    val city: String,
    val country: String,
    val countryCode: String,
    val isp: String,
    val lat: Double,
    val lon: Double,
    val org: String,
    val query: String,
    val region: String,
    val regionName: String,
    val status: String,
    val timezone: String,
    val zip: String
) {
    fun convertToLocal(): IpLocalModel {
        return IpLocalModel(
            `as` = `as`,
            city = city,
            country = country,
            countryCode = countryCode,
            isp = isp,
            lat = lat,
            lon = lon,
            org = org,
            query = query,
            region = region,
            regionName = regionName,
            status = status,
            timezone = timezone,
            zip = zip
        )
    }
}