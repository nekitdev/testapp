package com.nekitdev.data.repository

import com.nekitdev.data.api.IpApi
import com.nekitdev.data.utils.map
import com.nekitdev.data.utils.mapToResult
import com.nekitdev.domain.models.CustomResult
import com.nekitdev.domain.models.localmodels.IpLocalModel
import com.nekitdev.domain.repository.IpRepository

class IpRepositoryImpl(
    private val api: IpApi
) : IpRepository {

    override suspend fun getDataByIp(ip: String): CustomResult<IpLocalModel> {
        return api.getDataByIp(ip).mapToResult().map { it.convertToLocal() }
    }
}