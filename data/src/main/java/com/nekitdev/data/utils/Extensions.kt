package com.nekitdev.data.utils

import com.nekitdev.domain.models.CustomResult
import com.nekitdev.domain.models.error.ErrorLocalModel
import org.json.JSONObject
import retrofit2.Response

fun <T> Response<T>.mapToResult(): CustomResult<T> {
    return if (this.isSuccessful) {
        CustomResult.Success(this.body()!!)
    } else {
        val jObjError = JSONObject(errorBody()?.string())
        val error = ErrorLocalModel().convertToModel(jObjError)
        CustomResult.Failure(error)
    }
}

inline fun <reified T, reified R> CustomResult<T>.map(function: (T) -> R): CustomResult<R> =
    when (this) {
        is CustomResult.Success -> CustomResult.Success(function(this.data))
        is CustomResult.Failure -> CustomResult.Failure(this.error)
    }