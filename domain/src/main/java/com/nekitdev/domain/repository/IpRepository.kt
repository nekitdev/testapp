package com.nekitdev.domain.repository

import com.nekitdev.domain.models.CustomResult
import com.nekitdev.domain.models.localmodels.IpLocalModel

interface IpRepository {
    suspend fun getDataByIp(ip: String): CustomResult<IpLocalModel>
}