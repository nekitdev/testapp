package com.nekitdev.domain.usecase

import com.nekitdev.domain.models.CustomResult
import com.nekitdev.domain.models.localmodels.IpLocalModel
import com.nekitdev.domain.repository.IpRepository

class IpUseCase(private val repository: IpRepository) {

    suspend fun getDataByIp(ip: String): CustomResult<IpLocalModel> {
        return repository.getDataByIp(ip)
    }
}